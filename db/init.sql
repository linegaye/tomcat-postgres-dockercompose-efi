DROP TABLE IF EXISTS personnes;
CREATE TABLE personnes (
    id SERIAL PRIMARY KEY,
    prenom VARCHAR(64),
    nomFamille VARCHAR(64)
);
INSERT INTO personnes (prenom, nomFamille) VALUES ('EFI', 'Academy');
INSERT INTO personnes (prenom, nomFamille) VALUES ('Formation', 'Docker' );

